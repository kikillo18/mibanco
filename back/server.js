var express = require ('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechukiko/collections/";
var mLabAPIKey = "apiKey=doPpDtAQcBRF3hJcIO7LHEYLOZIg849e";
var requestJson = require('request-json');

//variables del login
//var query = 'q={"id" : ' + body[0].id +'}';
/*var  resMLabPUT =
var errPUT =
var bodyPUT =
//fin variables del login
*/
app.listen(port);
console.log("API escuchando en el puerto beeeeppppppp " + port);

app.get('/apitechu/v2/users',
  function (req, res) {
    console.log("GET /apitechu/v2/users");
    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");
    httpClient.get("users?" + mLabAPIKey,
      function(err, resMLab, body) {
        var response = !err ? body : {
          "msg" : "Error obteniendo usuarios. "
        }
        res.send(response);
      }
    )
    }
);

app.get('/apitechu/v2/users/:id',
function(req, res)
{
 var id = req.params.id;
 var query = 'q={"id" : ' + id + '}';
 console.log(query);
 console.log("GET /apitechu/v2/users/:id");
 //console.log(query)
 httpClient = requestJson.createClient(baseMlabURL);
 console.log("cliente creado");

 httpClient.get("users?" + query + "&" + mLabAPIKey,
 function(err, resMLab, body) {
     if (err) {
       response = {
         "msg" : "Error obteniendo usuario."
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         response = body[0];
       } else {
         response = {
           "msg" : "Usuario no encontrado"
         };
         res.status(404);
       }
     }
     res.send(response);
   }
)
}
);
//////////////////////////////////////////////////
/*  dado un id, muestra las cuentas que tiene*/
/////////////////////////
app.get('/apitechu/v2/users/:id/account',
function(req, res)
{
  var id = req.params.id;
  var query = 'q={"id" : ' + id + '}';
  console.log(query)
  console.log("GET /apitechu/v2/users/:id/account");
  httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("account?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo cuenta."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body;
         } else {
           response = {
             "msg" : "cuenta no encontrado."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
)
}
);
////////////////////////
/* consultar movimientos dado  un iban en la tabla de movmientos
/// es igual que la consulta de cuentas para un usuario  */
app.get('/apitechu/v2/users/:iban/mvtos',
function(req, res)
{
  var iban = req.params.iban;
  var query = 'q={"iban" : "' + iban + '"}';
  console.log(query)
  console.log("GET /apitechu/v2/users/:iban/mvtos");
  httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");
  httpClient.get("movimientos?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo movimientos."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body;
           console.log("paso api ok")
           console.log(body[0])
         } else {
           response = {
             "msg" : "No tiene movimientos."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
)
}
);
///////////////////////////////////////////////////
//login
app.post('/apitechu/v2/login',
 function(req, res) {
   console.log("POST /apitechu/v2/login");
   var email = req.body.email;
   var password = req.body.password;
   var query = 'q={"email": "' + email + '", "password":"' + password +'"}';
   console.log("query es " + query);
   httpClient = requestJson.createClient(baseMlabURL);
   httpClient.get("users?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Login incorrecto, email y/o passsword no encontrados",
           //"idUsuario" : body[0].id
         }
         res.status(500); //añadi esta linea y al logar mal me responde login no correcto
         res.send(response);
       } else {
         console.log("Got a user with that email and password, logging in");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$set":{"logged":true}}';
         httpClient.put("users?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var response = {
               "msg" : "Usuario logado con éxito",
               "idUsuario" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
)
app.post('/apitechu/v2/logout/:id',
 function(req, res) {
   console.log("POST /apitechu/v2/logout/:id");
   var query = 'q={"id": ' + req.params.id + '}';
   console.log("query es " + query);
   httpClient = requestJson.createClient(baseMlabURL);
   httpClient.get("users?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Logout incorrecto, usuario no encontrado"
         }
         res.send(response);
       } else {
         console.log("Got a user with that id, logging out");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$unset":{"logged":""}}'
         httpClient.put("users?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var response = {
               "msg" : "Usuario deslogado con éxito",
               "idUsuario" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
)
/////////////////////////////////////////////


//////////////////////////////////
app.post('/apitechu/v2/users/altausu',
function(req, res)
{
  console.log("/apitechu/v2/users/altausuario");
 var maxId = 1;
 var newUser = {
    "id": 0,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": req.body.password,
    "gender": req.body.gender
   };
 var Exist = 'q={"email" : "' + req.body.email +'"  }';
 httpClient = requestJson.createClient(baseMlabURL);
 httpClient.get("users?" + Exist + "&" + mLabAPIKey,
 function(errExist, resExist, bodyExist) {
      if (errExist) {
        response = {
          "msg" : "Error alta"
        }
        res.status(500);
        res.send(response);

      } else {
        if (bodyExist.length > 0) {
          response = {
            "msg" : "Usuario ya existente."
          }
          res.send(response);
         } else {
                var query = 'f={"id":1}&s={"id":-1}&l=1';
                console.log(query);
                console.log("esto pa saber lo que es")
                httpClient.get("users?" + query + "&" + mLabAPIKey,
                function(err, resMLab, body) {
                if (err) {
                      response = {
                           "msg" : "Error servicio"
                      }
                     res.status(500);
                     res.send(response);
                     } else {
                             this.maxId = body[0].id;
                             newUser.id = this.maxId + 1
                             console.log(newUser)
                             httpClient.post("users?" + mLabAPIKey ,newUser,
                             function(errPost, resPost, bodyPost){
                             if (errPost) {
                                 response = {
                                       "msg" : "Error en alta de usuario"
                                 }
                                res.status(500);
                                res.send(response);
                              } else {
                                    response = {
                                        "msg" : 'OK Alta: ' + newUser.first_name+ ', ' + newUser.last_name
                                    }
                                    res.status(200);
                                    res.send(response);
                              }
                            }
                            );
                        }
               }
              );
      }
    }
  }
)
}
);

///////este lo usamos para obtener con el IBAN de la colección account el saldo de la cuenta orígen y ver si es posible hacer la
//// transferencia

app.post('/apitechu/v2/users/saldo/:Iban',
function(req, res)
{
console.log("GET /apitechu/v2/users/saldo/:Iban");
  var Iban = req.params.Iban;
  console.log("vamos a obtener el saldo del iban")
  var query = 'q={"Iban" : "' + Iban + '"}';
  console.log(query)
  httpClient = requestJson.createClient(baseMlabURL);
  console.log("vamos por el saldo para comprobar que se puede hacer la Tx");
  httpClient.get("account?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo movimientos."
         }
         res.status(500);
       } else {
         console.log("verifico lengt y el body  pasado");
         console.log(body.length);
         console.log(body);
         if (body.length > 0) {
           response = body;
           console.log("paso api ok")
           console.log(body[0])
         } else {
           response = {
             "msg" : "No tiene movimientos."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
)
}
);
/////////////////**********//////
//////////////////////////////////

app.post('/apitechu/v2/users/transferir',
function(req, res)
{
  console.log("/apitechu/v2/users/transferir");
  var apunteorigen = { "iban": req.body.Iban_origen, "fecha": req.body.fecha, "concepto": "cargo transferencia", "cantidad": req.body.cantidad };
  console.log("apunteorigen: "); console.log(apunteorigen);
  var apuntedestino = { "iban": req.body.Iban_destino, "fecha": req.body.fecha, "concepto": "ingreso transferencia", "cantidad": req.body.cantidad };
  console.log("apuntedestino: "); console.log(apuntedestino);
  httpClient = requestJson.createClient(baseMlabURL);
  httpClient.post("movimientos?" + mLabAPIKey ,apunteorigen,
  function(errPost, resPost, bodyPost){
  if (errPost) {
      response = { "msg" : "Error en apunte orígen" }
     res.status(500);
     res.send(response);
   } else {
           httpClient.post("movimientos?" + mLabAPIKey, apuntedestino,
             function(errPost2, resPost2, bodyPost2) {
               if (errPost2){
                   response = { "msg" : "Error en apunte destino" }
                res.status(500);
                res.send(response);
               } else {
                     response = { "msg" : "Tx apuntada en orígen y destino" }
                     res.status(200);
                     res.send(response);
                }
            }
          )
      }
    }
  )
}
);
//
app.post('/apitechu/v2/users/ponersaldo',
function(req,res)
  {
   console.log("/apitechu/v2/users/restarsaldo");
   var Iban = req.body.Iban;
   var Saldo = req.body.balance;
   console.log("vamos a setear el saldo del iban")
   var query = 'q={"Iban" : "' + Iban + '"}';
   console.log(query)
   httpClient = requestJson.createClient(baseMlabURL);
   var putBody = '{"$set":{"balance": ' + Saldo + '}}';
// var putBody = '{"$set":{"logged":true}}';
   console.log(putBody);
   httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
     function(errPUT, resMLabPUT, bodyPUT) {
       if (errPUT){
         response = { "msg" : "Error Iban no encontrado"};
         res.status(500);
         res.send(response);
       }else {
       console.log("PUT done");
       var response = { "msg" : "Saldo actualizado tras la Tx" };
       res.send(response);
       }
     }
    )
 }
);

/******** fin apis ****///
